{- 
 -
 -                4
 - pi = -------------------
 -                1^2
 -       1 + --------------
 -                   3^2
 -            2 + ---------
 -                     5^2
 -                 2 + ----		
 -                      ...
 -}
 
pi1 n = 4 / (1+1/ recproc 2 n) where
  recproc x 0 = 2
  recproc x y = 2+(2*x-1)^2/recproc (x+1) (y-1)

{-
 -                  1^2
 - pi = 3 + -------------------
 -                   3^2
 -           6 + -------------
 -                     5^2
 -                6 + ------
 -                     ...
 -}
pi2 n = 3+reproc 1 n where
  reproc x 0 = (2*x-1)^2/6
  reproc x y = (2*x-1)^2/(6+reproc (x+1) (y-1))
{-
 -                 4
 - pi = ------------------------
 -                   1^2
 -       1 + ------------------
 -                    2^2
 -            3 + -----------
 -                     3^2
 -                 5 + ---
 -                     ...
 -}
pi3 n = 4/reproc 1 n where
  reproc x 0 = 2*x-1
  reproc x y = (2*x-1)+(x^2/reproc (x+1) (y-1))
 
{-       4     4     4     4
 - pi = --- - --- + --- - --- + ...
 -       1     3     5     7
 -}
pi4 n
  |n == 0 = 4
  |True = (-1)^n*4/(2*(fromIntegral n)+1)+pi4 (n-1)

{-             4         4         4         4
 - pi = 3 + ------- - ------- + ------- - -------- + ...
 -           2*3*4     4*5*6     6*7*8     8*9*10
 -}
pi5 n
  |n == 0 = 3
  |True = (-1)^(n+1)*4/(2*k*(2*k+1)*(2*k+2))+pi5 (n-1) where
     k = fromIntegral n

{-
       x^1     x^2
e^x = ----- + ----- + ...
        1!      2!
-}
e x 0 = 1
e x n = x^n/(factorial.fromIntegral) n + e x (n-1) where
  factorial 0 = 1
  factorial n = n*factorial (n-1)