
-- Функция декодирования двоичной записи числа
-- (аналогично декодированию азбуки Морзе, но
-- учтите бесконечность дерева -- нужна ленивость)
decodeBinary :: String -> Integer
decodeBinary str = dB 0 str where
	dB num [] = num
	dB num ('0':xs) = dB (2*num) xs
	dB num ('1':xs) = dB (2*num+1) xs	

-- Функция декодирования записи числа в системе
-- Фибоначчи: разряды -- числа Фибоначчи, нет
-- двух единиц подряд:
--    0f = 0
--    1f = 1
--   10f = 2
--  100f = 3
--  101f = 4
-- 1000f = 5
-- 1001f = 6
-- 1010f = 7
--   .....
-- (аналогично декодированию азбуки Морзе, но
-- учтите бесконечность дерева -- нужна ленивость)
decodeFibo :: String -> Integer
decodeFibo str = dF 0 1 (reverse str) where
	dF prev curr ('1':xs) = prev + curr + dF curr (prev + curr) xs
	dF prev curr ('0':xs) = dF curr (prev + curr) xs          
	dF _ _ [] = 0
              
