
-- | ���������� �������� � ��� ������ 
-- >>> myMaxIndex [1,5,2,3,4]
-- (1,5)
myMaxIndex :: [Integer] -> (Int,Integer)
myMaxIndex [x] = (0,x)
myMaxIndex (x:xs) = countIndex x 0 1 xs where
  countIndex currentMax maxIndex _ [] = (maxIndex,currentMax)
  countIndex currentMax maxIndex currentIndex (x:xs)
    |x>currentMax = countIndex x currentIndex (currentIndex+1) xs
    |True = countIndex currentMax maxIndex (currentIndex+1) xs

-- | ���������� ���������, ������ �������������
-- >>> maxCount [1,5,3,10,3,10,5]
-- 2
maxCount :: [Integer] -> Int
maxCount x = sum [1|y<-x,y == maxElem] where
  maxElem = foldl1 (\currentMax elem -> if elem>currentMax  then elem else currentMax) x

-- | ���������� ��������� ����� ����������� � ������������
-- >>> countBetween [-1,3,100,3]
-- 2
--
-- >>> countBetween [100,3,-1,3]
-- -2
--
-- >>> countBetween [-1,100]
-- 1
--
-- >>> countBetween [1]
-- 0
countBetween :: [Integer] -> Int
countBetween x = getIndex x (>) - getIndex x (<) where
  getIndex e f = fst $ foldl1 (\(currentIndex,chosenElem) (elemIndex,elem) ->
	 if f elem chosenElem then (elemIndex, elem) else (currentIndex,chosenElem)) $ zip [1..] e