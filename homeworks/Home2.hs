-- | deletes first arg from second arg
-- >>> deleteInt 1 [1,2,1,3,4]
-- [2,3,4]
--
-- >>> deleteInt 1 [2,3]
-- [2,3]
--
-- >>> deleteInt 1 []
-- []
deleteInt :: Int -> [Int] -> [Int]
deleteInt _ [] = []
deleteInt y (x:xs)
  |x == y = deleteInt y xs
  |True = x:deleteInt y xs
             
-- | returns list of indices of first arg in second arg
-- >>> findIndices 1 [1,2,1,3,4]
-- [0,2]
--
-- >>> findIndices 1 [2,3]
-- []
findIndicesInt :: Int -> [Int] -> [Int]
findIndicesInt x y = countPosition x y 0 where
  countPosition _ [] t = []
  countPosition y (x:xs) t
    |x == y = t:countPosition y xs (t+1)
    |True = countPosition y xs (t+1)

-- | tribo_n = tribo_{n-1} + tribo_{n-2} + tribo_{n-3}
--   tribo_0 = 1
--   tribo_1 = 1
--   tribo_2 = 1
--
-- >>> tribo 0
-- 1
--
-- >>> tribo 1
-- 1
--
-- >>> tribo 2
-- 1
--
-- >>> tribo 3
-- 3
--
-- >>> odd (tribo 100)
-- True
tribo n
  |n<=2 = 1
  |True = tribo (n-1) + tribo (n-2) + tribo (n-3)
-- ��� ����, ������� ����� ���� �����, ������, �������, ������, �����, ���� ����� �� ��� ����� (0-255)
-- �������� �������� :: ���� -> ���� -> ����
-- (������ ������������ �������������, ���� ���������� >255, �� �������� 255)
-- ��������������������, �������������������, ������������������ :: ���� -> Int

--data Colour = White | Black | Red | Green | Blue | RGB Int Int Int deriving Show
