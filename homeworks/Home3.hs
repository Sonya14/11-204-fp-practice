data Expr = Val Int
          | Plus Expr Expr
          | Minus Expr Expr
          | Mult Expr Expr
          | Div Expr Expr
          | Mod Expr Expr

eval :: Expr -> Int
eval (Val x) = x
eval (expr1 `Plus` expr2) = (eval expr1) + (eval expr2)
eval (expr1 `Minus` expr2) = (eval expr1) - (eval expr2)
eval (expr1 `Mult` expr2) = (eval expr1) * (eval expr2)
eval (expr1 `Div` expr2) = (eval expr1) `div` (eval expr2)
eval (expr1 `Mod` expr2) = (eval expr1) `mod` (eval expr2)